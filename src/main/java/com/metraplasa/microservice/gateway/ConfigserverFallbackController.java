package com.metraplasa.microservice.gateway;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigserverFallbackController {

	@GetMapping("/configserveroffline")
	public Map<String, String> configserverOffline() {
		Map<String, String> hasil = new HashMap<>();
		hasil.put("success", "false");
		hasil.put("message", "configserver offline");
		return hasil;
	}
}
